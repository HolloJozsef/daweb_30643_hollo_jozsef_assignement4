import { Injectable } from '@angular/core';
import {Tech} from "../model/tech";
import {Com} from "../model/com";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class TechService {

  constructor(private http: HttpClient) { }

  getComms(){

    return this.http.get(`http://localhost:8000/api/comment`);

  }

  postCom(com: Com){
    return this.http.post('http://localhost:8000/api/comment', com);
  }

  get(){
    return sessionStorage.getItem('token');
  }


}
