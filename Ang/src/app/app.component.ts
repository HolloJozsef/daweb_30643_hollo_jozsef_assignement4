import { Component } from '@angular/core';
import {Router} from "@angular/router";
import {TranslateService} from '@ngx-translate/core';
import {AuthService} from "./Services/auth.service";
import {TokenService} from "./Services/token.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'front';

  public loggedIn: boolean;

  constructor(
    private Auth: AuthService,
    private router: Router,
    private Token: TokenService,
    private translate: TranslateService,

  ) { }

  ngOnInit() {
    this.Auth.authStatus.subscribe(value => this.loggedIn = value);
  }

  logout(event: MouseEvent) {
    event.preventDefault();
    this.Token.remove();
    sessionStorage.clear();
    this.Auth.changeAuthStatus(false);
    this.router.navigateByUrl('/login');



  }


  goAbout(){
    this.router.navigate(['/about']);
  }
  goContact(){
    this.router.navigate(['/contact']);
  }
  goCoordonator(){
    this.router.navigate(['/coordonator']);
  }
  goHome() {
    this.router.navigate(['/home']);
  }
  goNoutati(){
  this.router.navigate(['/noutati']);
  }
  goProfil(){
    this.router.navigate(['/profil']);
  }
  goLogin(){
    this.router.navigate(['/login']);
  }
  useLanguage(language: string) {
    this.translate.use(language);
  }
}

