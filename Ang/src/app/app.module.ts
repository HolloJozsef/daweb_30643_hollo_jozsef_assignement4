import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule,ReactiveFormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { NoutatiComponent } from './noutati/noutati.component';
import { AppRoutingModule } from './app-routing.module';
import { RouterModule, Routes } from '@angular/router';
import { ContactComponent } from './contact/contact.component';
import { HomeComponent } from './home/home.component';
import { ProfilComponent } from './profil/profil.component';
import { AboutComponent } from './about/about.component';
import { CoordonatorComponent } from './coordonator/coordonator.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatToolbarModule } from '@angular/material/toolbar';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import {JarwisService} from "./Services/jarwis.service";
import {TokenService} from "./Services/token.service";
import {AuthService} from "./Services/auth.service";
import {AfterLoginService} from "./Services/after-login.service";
import {BeforeLoginService} from "./Services/before-login.service";

import { AvatarComponent } from './avatar/avatar.component';
import {TechService} from "./Services/tech.service";
import {MatDialogModule, MatDialogRef} from "@angular/material/dialog";
import {AgmCoreModule} from "@agm/core";
import { MapComponent } from './map/map.component';
import {AgmDirectionModule} from "agm-direction";

@NgModule({
  declarations: [
    AppComponent,
    NoutatiComponent,
    ContactComponent,
    HomeComponent,
    ProfilComponent,
    AboutComponent,
    CoordonatorComponent,
    LoginComponent,
    SignupComponent,
    UserProfileComponent,
    AvatarComponent,
    MapComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatFormFieldModule, MatButtonModule, MatInputModule, MatToolbarModule, MatSelectModule, MatDialogModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyA3WxNmHN-fP1Lz2cpie-Y6fpILIUyf5j4'
    }), AgmDirectionModule

  ],
  providers: [
    JarwisService,
    TokenService,
    AuthService,
    AfterLoginService,
    BeforeLoginService,
    TechService,
    {
      provide: MatDialogRef,
      useValue: {}
    },

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

