import { Component, OnInit } from '@angular/core';
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-coordonator',
  templateUrl: './coordonator.component.html',
  styleUrls: ['./coordonator.component.css']
})
export class CoordonatorComponent implements OnInit {

  constructor(private translate: TranslateService) {
    translate.setDefaultLang('en');
  }
  ngOnInit() {
  }

}
