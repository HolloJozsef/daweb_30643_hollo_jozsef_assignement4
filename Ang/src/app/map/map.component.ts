import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {
  title: string = 'AGM project';
  latitude: number;
  longitude: number;
  zoom:number
  constructor() { }
  public lat = 46.771248;
  public lng = 23.623621;

  public origin: any;
  public destination: any;
  ngOnInit(): void {
    this.getDirection();
  }
  private setCurrentLocation() {

      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 15;
      });

  }

  getDirection() {
    this.origin = { lat: 24.799448, lng: 45.979021 };
    this.destination = { lat: 24.799524, lng: 45.975017 };

    // this.origin = 'Taipei Main Station';
    // this.destination = 'Taiwan Presidential Office';
  }
}
