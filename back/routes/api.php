<?php
Route::group([

    'middleware' => 'api',


], function ($router) {

    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
    Route::post('avatar/{id}', 'AuthController@avatar');
    Route::post('comment', 'comments@postCom');
    Route::get('comment', 'comments@getCom');

});
